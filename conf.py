# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
import os
import sys
import time

sys.path.append(os.path.abspath('_ext'))

import versioneer

__version__ = versioneer.get_versions()['version']

del versioneer

# -- Project information -----------------------------------------------------

project = 'SatNOGS'
copyright = u'2014-{}, Libre Space Foundation'.format(time.strftime('%Y'))
author = u'SatNOGS'
version = __version__
release = __version__

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.todo',
    'sphinx.ext.intersphinx',
    'sphinx.ext.graphviz',
    'gitlab_api',
]

suppress_warnings = ['epub.unknown_project_files']

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store', '.tox']

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'
html_theme_options = {
    'logo_only': True,
    "style_nav_header_background": "#5b5b66",
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
html_logo = 'logo.svg'
html_css_files = ['css/custom.css']

html_context = {
    "display_gitlab": True,
    "gitlab_user": "librespacefoundation",
    "gitlab_repo": "satnogs/satnogs-docs",
    "gitlab_version": "master",
    "conf_py_path": "/",
}

# Set canonical URL from the Read the Docs Domain
html_baseurl = os.environ.get('READTHEDOCS_CANONICAL_URL', '')

# Tell Jinja2 templates the build is running on Read the Docs
if os.environ.get('READTHEDOCS', '') == 'True':
    html_context['READTHEDOCS'] = True

linkcheck_workers = 10
linkcheck_timeout = 15

# -- Extension configuration -------------------------------------------------

# -- Options for todo extension ----------------------------------------------

# If true, `todo` and `todoList` produce output, else they produce nothing.
todo_include_todos = True

# -- Options for intersphinx extension ---------------------------------------

# Example configuration for intersphinx: refer to the Python standard library.
#intersphinx_mapping = {'python': ('https://docs.python.org/': None)}
intersphinx_mapping = {
    'satnogs-config':
    ('https://docs.satnogs.org/projects/satnogs-config/en/stable', None),
    'satnogs-client':
    ('https://docs.satnogs.org/projects/satnogs-client/en/stable', None),
    'satnogs-network':
    ('https://docs.satnogs.org/projects/satnogs-network/en/stable', None),
    'satnogs-db':
    ('https://docs.satnogs.org/projects/satnogs-db/en/stable', None),
}
